# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 11:22:05 2023

@author: wiesbrock
"""

import os
import csv

def merge_csv_files(input_folder, output_file):
    # Liste alle CSV-Dateien im Eingabeordner auf
    csv_files = [f for f in os.listdir(input_folder) if f.endswith('.csv')]

    if not csv_files:
        print("Keine CSV-Dateien im Eingabeordner gefunden.")
        return

    # Öffne die Ausgabedatei im Schreibmodus
    with open(output_file, 'w', newline='') as output_csv:
        writer = csv.writer(output_csv)

        # Durchlaufe alle CSV-Dateien im Eingabeordner
        for idx, csv_file in enumerate(csv_files):
            with open(os.path.join(input_folder, csv_file), 'r', newline='') as input_csv:
                reader = csv.reader(input_csv)
                header = next(reader) if idx == 0 else None

                # Schreibe den Header nur bei der ersten Datei
                if idx == 0:
                    writer.writerow(header)

                # Schreibe den Inhalt in die Ausgabedatei (ohne Header)
                if idx > 0:
                    for row in reader:
                        writer.writerow(row)

    print("Zusammenführen der CSV-Dateien abgeschlossen.")

# Beispielaufruf
input_folder = 'D:\Ponzo new\Catch'
output_file = 'D:\Ponzo new\Catch/Ausgabedatei.csv'
merge_csv_files(input_folder, output_file)
