# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 11:29:38 2023

@author: wiesbrock
"""

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import zscore
import seaborn as sns
import os
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans

path=r'D:\Ponzo new\Catch\Ausgabedatei.csv'
data=pd.read_csv(path)
choice=data['last_choice']
catch=data['catch_trial']
correct=data['correctstim']
#correct=correct[catch=='2']
correct=correct[correct!='correctstim']
correct=correct.dropna()
correct=np.array(correct)

correct=correct.astype(str)


print(len(correct[correct=='right'])/len(correct))
print(len(correct[correct=='left'])/len(correct))

choice=np.array(choice)
catch=np.array(catch)

catch_1=choice[catch=='1']
catch_2=choice[catch=='2']

catch_1_left=len(catch_1[catch_1=='left'])
catch_1_right=len(catch_1[catch_1=='right'])

catch_2_left=len(catch_2[catch_2=='left'])
catch_2_right=len(catch_2[catch_2=='right'])

catch_1_left_rat=catch_1_left/len(catch_1)
catch_2_left_rat=catch_2_left/len(catch_2)

catch_1_right_rat=catch_1_right/len(catch_1)
catch_2_right_rat=catch_2_right/len(catch_2)

from statsmodels.stats.proportion import proportion_confint



catch_0=choice[catch=='0']

catch_0_left=len(catch_0[catch_0=='left'])
catch_0_right=len(catch_0[catch_0=='right'])


catch_0_left_rat=catch_0_left/len(catch_0)
catch_0_right_rat=catch_0_right/len(catch_0)

from statsmodels.stats.proportion import proportion_confint

liste_lower=[]
liste_upper=[]
#calculate 95% confidence interval with 56 successes in 100 trials
liste_lower.append(proportion_confint(count=len(catch_0)/2, nobs=len(catch_0))[0])
liste_upper.append(proportion_confint(count=len(catch_0)/2, nobs=len(catch_0))[1])



liste_lower.append(proportion_confint(count=len(catch_1)/2, nobs=len(catch_1))[0])
liste_upper.append(proportion_confint(count=len(catch_1)/2, nobs=len(catch_1))[1])

liste_lower.append(proportion_confint(count=len(catch_2)/2, nobs=len(catch_2))[0])
liste_upper.append(proportion_confint(count=len(catch_2)/2, nobs=len(catch_2))[1])

plt.figure(dpi=300)
plt.bar(np.arange(3),(catch_0_right_rat*100,catch_1_right_rat*100,catch_2_right_rat*100))
plt.hlines(y=liste_lower[0]*100,xmin=-0.5,xmax=0.5,color='k')
plt.hlines(y=liste_upper[0]*100,xmin=-0.5,xmax=0.5,color='k')

plt.hlines(y=liste_lower[1]*100,xmin=0.5,xmax=1.5,color='k')
plt.hlines(y=liste_upper[1]*100,xmin=0.5,xmax=1.5,color='k')

plt.hlines(y=liste_lower[2]*100,xmin=1.5,xmax=2.5,color='k')
plt.hlines(y=liste_upper[2]*100,xmin=1.5,xmax=2.5,color='k')


plt.xticks(np.arange(3),('Ctrl','Stim_1','Stim_2'))
plt.ylabel('Percent of repsonses to the right side')
plt.ylim(40,60)
sns.despine()
